﻿#include <iostream>
#include <string>
#include <list>
#include <algorithm>

class Identifier // Клас "Ідентификатор"
{
public:
    Identifier(const std::string& name) :
        m_name(name)
    {
    }

public:
    std::string name() const
    {
        return m_name;
    }

private:
    std::string m_name;
};

bool operator==(const Identifier& left, const Identifier& right) // Необхідно для пошуку змінної по імені
{
    return left.name() == right.name();
}

size_t hash(const Identifier& id)
{
    // Функція, обчислююча хеш
    // Приймае ідентификатор, хеш якого потрібно порахувати та повертає її
    if (id.name().length() == 1)
        return 2 * size_t(id.name()[0]);
    return size_t(id.name()[0]) + size_t(id.name()[1]);
}

// Клас виключення, якщо ідентифікатор не знайдений 
// Виводить повідомлення про те, що ідентификатор не знайдений в таблиці
class IDNotFoundException : std::exception
{
public:
    IDNotFoundException(const std::string id_name) :
        m_what(std::string("Identifier \'") + id_name + "\' not found!")
    {
    }

    virtual ~IDNotFoundException() throw()
    {
    }

public:
    const char* what() const throw()
    {
        return m_what.c_str();
    }

private:
    std::string m_what;
};

// Клас "Хеш-таблиця", з ланцюжками
class HashTable
{
public:
    static const size_t min_hash_value = int('A') + int('0');
    static const size_t max_hash_value = int('z') + int('z');
    static const size_t hash_table_size = max_hash_value - min_hash_value;

public:
    void add(const Identifier& id)
    {
        m_hash_table[hash(id) - min_hash_value].push_back(id);
    }

    // Поиск ідентификатора в таблиці по імені
    Identifier& get(const std::string& id_name)
    {
        std::list<Identifier>& line = m_hash_table[hash(id_name) - min_hash_value];

        std::list<Identifier>::iterator it =
            std::find(line.begin(), line.end(), id_name);

        if (it == line.end())
            throw IDNotFoundException(id_name);

        return *it;
    }

private:
    std::list<Identifier> m_hash_table[hash_table_size];
};

int main()
{
    // Створюємо хеш-таблицю
    HashTable ht;

    ht.add(Identifier("b"));
    ht.add(Identifier("bb"));
    ht.add(Identifier("ac"));
    ht.add(Identifier("ca"));

    try
    {
        // Выводим на екран інформацию 
        std::cout << ht.get("b").name() << std::endl;
        std::cout << ht.get("bb").name() << std::endl;
        std::cout << ht.get("ac").name() << std::endl;
        std::cout << ht.get("ca").name() << std::endl;
        std::cout << ht.get("Algo").name() << std::endl;
    }
    catch (const IDNotFoundException& ex)
    {
        std::cerr << ex.what() << std::endl;
    }

    return 0;
}